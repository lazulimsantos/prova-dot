<?php
  for ($i=1; $i<=100 ; $i++) { 
    if (($i%3)===0 && ($i%5)===0) {
      print('FizzBuzz<BR>');
      continue;
    }
    if (($i%3)===0){
      print('Fizz<BR>');
      continue;
    }
    if (($i%5)===0){
      print('Buzz<BR>');
      continue;
    }
    print($i.'<BR>');
  }

?>