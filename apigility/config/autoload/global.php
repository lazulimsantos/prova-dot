<?php
return [
    'zf-content-negotiation' => [
        'selectors' => [],
    ],
    'db' => [
        'adapters' => [
            'dummy' => [],
            'Db/Formatura' => [],
            'Db/Tarefas' => [],
        ],
    ],
];
