<?php
return [
    'router' => [
        'routes' => [
            'dot_api.rest.task' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/task[/:id]',
                    'defaults' => [
                        'controller' => 'dot_api\\V1\\Rest\\Task\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'dot_api.rest.task',
        ],
    ],
    'zf-rest' => [
        'dot_api\\V1\\Rest\\Task\\Controller' => [
            'listener' => 'dot_api\\V1\\Rest\\Task\\TaskResource',
            'route_name' => 'dot_api.rest.task',
            'route_identifier_name' => 'id',
            'collection_name' => 'task',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'POST',
                4 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
                2 => 'PUT',
                3 => 'DELETE',
                4 => 'PATCH',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \dot_api\V1\Rest\Task\TaskEntity::class,
            'collection_class' => \dot_api\V1\Rest\Task\TaskCollection::class,
            'service_name' => 'task',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'dot_api\\V1\\Rest\\Task\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'dot_api\\V1\\Rest\\Task\\Controller' => [
                0 => 'application/vnd.dot_api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'dot_api\\V1\\Rest\\Task\\Controller' => [
                0 => 'application/vnd.dot_api.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \dot_api\V1\Rest\Task\TaskEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'dot_api.rest.task',
                'route_identifier_name' => 'id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \dot_api\V1\Rest\Task\TaskCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'dot_api.rest.task',
                'route_identifier_name' => 'id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-apigility' => [
        'db-connected' => [
            'dot_api\\V1\\Rest\\Task\\TaskResource' => [
                'adapter_name' => 'Db/Tarefas',
                'table_name' => 'task',
                'hydrator_name' => \Zend\Hydrator\ArraySerializable::class,
                'controller_service_name' => 'dot_api\\V1\\Rest\\Task\\Controller',
                'entity_identifier_name' => 'id',
                'table_service' => 'dot_api\\V1\\Rest\\Task\\TaskResource\\Table',
            ],
        ],
    ],
    'zf-content-validation' => [
        'dot_api\\V1\\Rest\\Task\\Controller' => [
            'input_filter' => 'dot_api\\V1\\Rest\\Task\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'dot_api\\V1\\Rest\\Task\\Validator' => [
            0 => [
                'name' => 'titulo',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '255',
                        ],
                    ],
                ],
            ],
            1 => [
                'name' => 'descricao',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '255',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'dot_api-V1-Rest-Task-Controller' => [
                'collection' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
                'entity' => [
                    'GET' => true,
                    'POST' => true,
                    'PUT' => true,
                    'PATCH' => true,
                    'DELETE' => true,
                ],
            ],
        ],
    ],
];
