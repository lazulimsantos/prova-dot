Prova de Analista Desenvolvedor Pleno

Instru��es para executar os programas.

Instalar na sua m�quina os programas Apache, MySQL, PHP para a execu��o dos projetos.

Para desenvolver foi utilizado o aplicativo MAMP rodando o server Apache.

Para execu��o dos exerc�cios 1, 2 e 3 baixe a pasta teste_dot e coloque no diret�rio do server, geralmente com os nomes de htdocs ou www.
Em seguida basta iniciar o Apache e acessar a url da aplica��o, localmente � http://localhost:[porta]/teste_dot e escolher qual exerc�cio deseja executar.

Para desenvolver exerc�cio 4 foi utilizado o frameword Apibility, desenvolvido pela ZEND, � um servi�o para desenvolvimento de API REST ou RPC totalmente simples e amig�vel. Cont�m uma interface gr�fica que auxilia e facilita a cria��o de APIs.
Para mais informa��es acesse a p�gina do framework https://apigility.org/ .
Para executar o projeto baixe a pasta apigility e copie para um diret�rio de sua prefer�ncia, n�o precisa ser no server pois o Apibility disponibiliza um server.

Para iniciar o Apibility acesse o diret�rio do projeto pelo terminal e execute o seguinte comando:
php -S 0.0.0.0:8888 -ddisplay_errors=0 -t public public/index.php

Pronto! A API j� est� dispon�vel pelo endere�o http://localhost:8888/task/.